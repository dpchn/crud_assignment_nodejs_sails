/**
 * Student.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	name : {
  		type : "string"
  	},
  	email  :{
  		type : "string"
  	}
  },

  add : function( data  , cb  ) {

  		Student.findOne({ email : data.email }).exec(function (err, studentData){
  			if(!err && studentData){
  				cb(null, studentData);
  			}else if(  studentData == undefined  ){
  				Student.create(data).exec(function (err, newStudentData){
  						if(!err && newStudentData){
  							cb(null, newStudentData);
  						}
  				});
  			}else{
  				cb(err);
  			}
  		});

  },

  get : function(data, cb ) {

  }



};




