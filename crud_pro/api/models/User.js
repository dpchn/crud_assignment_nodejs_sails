/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    firstname : { type: 'string' },

    lastname : { type: 'string' },

    rollnumber : { type: 'string' },

    email : { type: 'string' },

    password : { type: 'string' }
  },


  signup: function(data, cb){
  	User.findOne({$or:[ {email : data.email}, {rollnumber: data.rollnumber}]} ).exec(function (err, result){
  	  if (!err && result) {
  	    cb("Email or Rollnumber Exist", null);
  	  }
  	  else if(result==undefined){
  	  	User.create(data).exec(function (err, newresult){
  	  	  if (!err && newresult){
  	  	   cb("Succefully Regsiter", null); }
  	  	  
  	  	});
  	  }
  	  else{
  	  	cb(err);
  	  }
  	  
    });
  },



  login: function(data, cb){
    sails.log("TESR K");
  	User.findOne({$and:[{rollnumber: data.rollnumber},{password : data.password}]}).exec(function(err, result){
  		if(!err && result){
			   cb("Succefully login");
  		}
  		else if(err){
        cb(err);
  		}
  		else
  			cb("Rollnumber/Password wrong");
  	});
  },


  show: function(cb){
    User.find().exec(function(err, result){
      if(!err && result){
        cb(result);
      }
      else{
        cb(err);
      }
    });
  },


  del: function(data, cb){
    User.destroy({"rollnumber":data.rollnumber}).exec(function(err, result){
      if(!err && result){
        cb(result);
      }
      else{
        cb(err);
      }
    });
  },




  get: function(data, cb){
      User.findOne({rollnumber: data.rollnumber}).exec(function(err, result){
        if(!err && result){
          cb(result);
        }
        else{

          cb(err);
        }
      });
  },



  edit: function(data, cb){;
      User.update({"rollnumber": data.body.rollnumber},{"firstname": data.body.firstname, "lastname": data.body.lastname, "email": data.body.email}).exec(function(result, err){
        
        if(!err && result){
          cb(result);
        }
        else{
          cb(err);
        }
      });
  },



};




