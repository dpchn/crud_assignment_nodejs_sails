/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `UserController.index()`
   */
  index: function (req, res) {
    return res.json({
      todo: 'index() is not implemented yet!'
    });
  },

  /**
   * `UserController.create()`
   */
  signup: function (req, res) {
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var rollformat = /^[0-9]+$/;
            var firstnameformat = /^[a-zA-Z]+$/;
            var lastnameformat = /^[a-zA-Z]+$/;
            if(!req.body.firstname || !req.body.lastname || !req.body.email || !req.body.rollnumber || !req.body.password){
                return res.badRequest({"message" : "Details are missing"});
                //res.view("signup",{result:"missing"});
            }
            else if(!req.body.email.match(mailformat)){ 
                return res.badRequest({"message" : "Email not valid"});   
              }
            else if(!req.body.rollnumber.match(rollformat)){
                return res.badRequest({"message" : "Enter valid rollnumber"});
            }
            else if(!req.body.firstname.match(firstnameformat)){
                return res.badRequest({"message" : "Enter valid First Name"});
            }
            else if(!req.body.firstname.match(firstnameformat)){
                return res.badRequest({"message" : "Enter valid Last Name"});
            }
            else if(req.body.password.length<5){
                return res.badRequest({"message" : "Password length at least 6"});
            }
              else{    
                     User.signup({firstname : req.body.firstname, lastname : req.body.lastname, email : req.body.email, rollnumber : req.body.rollnumber, password : req.body.password}, function(result, err){
                if(!err && result){
                    res.view("notification",{result:result});
                }
                else{
                    res.negotiate(err);
                }
            });
      }
  },



    /**
   * `UserController.login()`
   */
  login: function (req, res) {
    if(!req.body.rollnumber || !req.body.password){
        return res.badRequest({"message": "rollnumber/password missing"});
    }
    else{
        User.login({rollnumber : req.body.rollnumber, password : req.body.password},function(result,err){
            if(!err && result){
              res.redirect('/show');
            }
            else{
                res.negotiate(err);
            }
        });
    }
  },



  /**
   * `UserController.show()`
   */
  show: function (req, res) {
      User.show(function(result, err){
        if(!err && result){
          res.view("alldata",{result:result});
        }
        else{
          res.negotiate({"err":"Error"});
        }
      });
  },


  /**
   * `UserController.edit()`
   */




   addnew: function (req, res) {
             var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var rollformat = /^[0-9]+$/;
            var firstnameformat = /^[a-zA-Z]+$/;
            var lastnameformat = /^[a-zA-Z]+$/;
            if(!req.body.firstname || !req.body.lastname || !req.body.email || !req.body.rollnumber || !req.body.password){
                return res.badRequest({"message" : "Details are missing"});
                //res.view("signup",{result:"missing"});
            }
            else if(!req.body.email.match(mailformat)){ 
                return res.badRequest({"message" : "Email not valid"});   
              }
            else if(!req.body.rollnumber.match(rollformat)){
                return res.badRequest({"message" : "Enter valid rollnumber"});
            }
            else if(!req.body.firstname.match(firstnameformat)){
                return res.badRequest({"message" : "Enter valid First Name"});
            }
            else if(!req.body.firstname.match(firstnameformat)){
                return res.badRequest({"message" : "Enter valid Last Name"});
            }
            else if(req.body.password.length<5){
                return res.badRequest({"message" : "Password length at least 6"});
            }
            else{              
               User.signup({firstname : req.body.firstname, lastname : req.body.lastname, email : req.body.email, rollnumber : req.body.rollnumber, password : req.body.password}, function(result, err){
              if(!err && result){
                res.redirect('/show');
              } 
            else{
                res.negotiate(err);
              }
        });
      }
  },



  getforupdate: function(req, res){
    if(!req.params.input)
      return res.badRequest({"message":"Not rollnumber"});
      
    User.get({rollnumber : req.params.input}, function(result, err){
      if(err)
        return res.negotiate(err);
      
      res.view("doupdate",{result:result});
    });
  },





  update: function (req, res) {
      if(!req.body.firstname)
          return res.badRequest({"message":"Not rollnumber"});
      
   User.edit(req , function(result, err){
          if(!err && result)
            res.redirect('/show');
          else
            res.negotiate(err);
        });
  },


  /**
   * `UserController.delete()`
   */
  delete: function (req, res) {
      if(!req.params.input){
        return res.badRequest({"message":"Not fund"});
      }
      else{
        User.del({rollnumber : req.params.input}, function(result, err){
          if(!err && result){
            res.redirect('/show');
          }
          else{
            res.negotiate(err);
          }
        });
      }
  },
};

