/**
 * StudentController
 *
 * @description :: Server-side logic for managing students
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	add : function(req, res) {
			if(!req.body.email || !req.body.name)
				return res.badRequest({ "message" : "email/name missing" });

			Student.add( {  email : req.body.email , name : req.body.name   } , function(err, result) {
				if(!err && result){
					res.send(result);
				}else{
					res.negotiate(err);
				}

			});
	}
};

